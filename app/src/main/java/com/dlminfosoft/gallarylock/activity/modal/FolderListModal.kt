package com.dlminfosoft.gallarylock.activity.modal

data class FolderListModal(val name: String, val totalItem: Int)